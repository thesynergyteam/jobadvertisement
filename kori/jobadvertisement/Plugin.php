<?php

namespace KoRi\JobAdvertisement;

use Backend\Facades\Backend;
use KoRi\JobAdvertisement\Components\RenderJobs;
use KoRi\JobAdvertisement\Components\JobSlug;
use KoRi\JobAdvertisement\Models\Job;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

/**
 * Class Plugin
 * @package KoRi\JobAdvertisement
 */
class Plugin extends PluginBase
{

//    public $elevated = true;

    /**
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'kori.jobadvertisement::lang.plugin.name',
            'description' => 'kori.jobadvertisement::lang.plugin.description',
            'author' => 'KoRi',
            'icon' => 'icon-code',
        ];
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        \Event::listen('offline.sitesearch.query', function ($query) {

            $jobs = Job::where('title', 'like', "%${query}%")
//                ->orWhere('description', 'like', "%${query}%")
                ->where('is_active',1)
                ->get();

            $results = array();

            foreach ($jobs as $job) {
                $arrayItem = array(
                    'title' => $job->title,
                    'text' => $job->description,
                    'url' => '/job/' . $job->slug,
                    'relevance' => 'Álláshirdetés',
                    'model' => $job
                );
                array_push($results, $arrayItem);
            }
            return [
                'provider' => '(álláshirdetés)',
                'results' => $results,
            ];
        });

    }

    /**
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'jobadvertisement' => [
                'label' => 'kori.jobadvertisement::lang.navigation.jobadvertisement',
                'url' => Backend::url('kori/jobadvertisement/jobs'),
                'icon' => 'icon-newspaper-o',
                'permissions' => ['kori.jobadvertisement.*'],
                'order' => 500,
                'sideMenu' => [
                    'jobs' => [
                        'label' => 'kori.jobadvertisement::lang.navigation.jobs',
                        'icon' => 'icon-inbox',
                        'url' => Backend::url('kori/jobadvertisement/jobs'),
                        'permissions' => ['kori.jobadvertisement.*'],
                    ],
                    'jobcats' => [
                        'label' => 'kori.jobadvertisement::lang.navigation.jobcats',
                        'icon' => 'icon-inbox',
                        'url' => Backend::url('kori/jobadvertisement/jobcats'),
                        'permissions' => ['kori.jobadvertisement.access_jobs'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerPermissions()
    {
        return [
/*            'kori.jobadvertisement.access_settings' => [
                'label' => 'kori.jobadvertisement::lang.permissions.access_settings',
                'tab' => 'kori.jobadvertisement::lang.permissions.tab',
            ],
*/            'kori.jobadvertisement.access_jobs' => [
                'label' => 'kori.jobadvertisement::lang.permissions.access_jobs',
                'tab' => 'kori.jobadvertisement::lang.permissions.tab',
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            RenderJobs::class => 'renderJobs',
            JobSlug::class => 'jobSlug',
        ];
    }

    /**
     * @return array
     */
    public function registerPageSnippets()
    {
        return [
            RenderJobs::class => 'renderJobs',
            JobSlug::class => 'jobSlug',
        ];
    }

}
