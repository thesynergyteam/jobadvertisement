<?php

namespace KoRi\JobAdvertisement\Components;

use Cms\Classes\ComponentBase;
use KoRi\JobAdvertisement\Models\Job as JobModel;
use KoRi\JobAdvertisement\Models\JobCats;
use \Cms\Classes\Page;

class RenderJobs extends ComponentBase {

    public function componentDetails() {
        // Nyelvesítés.
        return [
            "name" => "Állások",
            "description" => "Állásajánlatok megjelenítése."
        ];
    }

    public function defineProperties() {
        // Nyelvesítés.
        return [
            "jobsPageTitle" => [
                "title" => "Oldalcím",
                "description" => "Amennyiben megadsz egy oldalcímet, az megjelenik az állások felett. Hagyd üresen, ha nem szeretnéd, hogy megjelenjen.",
                "type" => "text"
            ],
            "titleAlign" => [
                "title" => "Oldalcím igazítása",
                "description" => "Csak abban az esetben van értelme a beállításnak, ha megjelenik az oldalcím.",
                "type" => "dropdown"
            ],
            "categoryId" => [
                "title" => "Kategória",
                "description" => "Amennyiben kiválasztasz egy kategóriát, kizárólag abból a kategóriából jelennek meg állásajánlatok.",
                "type" => "dropdown",
                "default" => "all"
            ],
            "orderBy" => [
                "title" => "Rendezési tulajdonság",
                "description" => "Válaszd ki, hogy melyik tulajdonságuk alapján akarod rendezni a megjelenítendő állásokat.",
                "type" => "dropdown",
                "options" => [
                    "title" => "Cím alapján",
                    //"author" => "Szerző szerint",
                    "published_at" => "Közzététel dátuma szerint",
                    "created_at" => "Létrehozás dátuma szerint",
                    "updated_at" => "Utolsó frissítés dátuma szerint"
                ],
                "default" => "created_at"
            ],
            "orderByType" => [
                "title" => "Rendezés módja",
                "description" => "Válaszd ki, hogy növekvő vagy csökkenő sorrendben akarod a fentebbi tulajdonság alapján listázni a híreket.",
                "type" => "dropdown",
                "options" => [
                    "asc" => "Növekvő",
                    "desc" => "Csökkenő"
                ],
                "default" => "desc"
            ],
            "showNum" => [
                "title" => "Megjelenő állások száma listában",
                "description" => "Az itt megadott mennyiségű állás fog megjelenni összesen. Ha 0-t írunk be, az összes állásajánlat meg fog jelenni.",
                "default" => 0,
                "validationPattern" => "^[0-9]+$",
                "validationMessage" => "A(z) „Megjelenő állások száma” mezőbe kizárólag számokat írhatsz be!"
            ],
        ];
    }

    public function getTitleAlignOptions() {
        return [
            "text-left" => "Balra igazított",
            "text-center" => "Középre igazított",
            "text-right" => "Jobbra igazított"
        ];
    }

    public function getCategoryIdOptions() {
        return ['all' => 'Az összes kategóriából'] + JobCats::orderBy("name")->lists("name", "id");
    }


    public function onRender() {
        if (is_numeric($this->property("categoryId"))) {
            $query = JobModel::whereHas("categories", function($q) {
                $q->where("cat_id", $this->property("categoryId"));
            })->where('is_active',1)->get();
        } else {
            $query = JobModel::where('is_active',1)->get();
        }

        if ($this->property("orderByType") == "asc") {
            $query = $query->sortBy($this->property("orderBy"));
        } else if ($this->property("orderByType") == "desc") {
            $query = $query->sortByDesc($this->property("orderBy"));
        }

        if ($this->property("showNum") > 0) {
            $query = $query->take(intval($this->property("showNum")));
        }

        $this->page["data"] = $query;

        foreach ($this->getProperties() as $key => $value) {
            $this->page[$key] = $value;
        }
    }

    /**
     * @inheritdoc
     */
    public function onRun()
    {
        //$this->addAssets();
    }

    /**
     * @return void
     */
    private function addAssets()
    {
        $this->addJs('assets/js/jobs.js');
        $this->addCss('assets/css/jobs.css');
    }


}
