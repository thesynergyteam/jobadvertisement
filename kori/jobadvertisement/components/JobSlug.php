<?php

namespace KoRi\JobAdvertisement\Components;

use Cms\Classes\ComponentBase;
use KoRi\JobAdvertisement\Models\Job;

class JobSlug extends ComponentBase
{
    public $record;

    public function componentDetails() {
        return [
            'name' => 'Paraméterezett állás',
            'description' => 'A böngésző URL címéből lekérdezett álláshirdetést jeleníti meg, ha létezik.'
        ];
    }

    public function init()
    {
        $this->record = Job::where('slug', $this->param('slug'))->first();
    }

    public function onRun()
    {
        $record = $this->record;
        $cats = $this->record->categories->pluck('slug')->toArray();
        switch(true){
            case (in_array('fkf',$cats)): $this->page['brandcolor'] = '#ED6B06'; break;
            case (in_array('bti',$cats)): $this->page['brandcolor'] = '#9E0933'; break;
            case (in_array('fotav',$cats)): $this->page['brandcolor'] = '#A2C753'; break;
            case (in_array('foketusz',$cats)): $this->page['brandcolor'] = '#706F6F'; break;
            default: $this->page['brandcolor'] = '#23753A'; break;
        }
        $this->page['record'] = $record;
        $this->page->semititle = $this->page['record']['title'];

        foreach ($this->getProperties() as $key => $value) {
            $this->page[$key] = $value;
        }
    }
}
