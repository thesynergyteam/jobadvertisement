<?php

namespace KoRi\JobAdvertisement\Models;

use Model;

/**
 * Model
 */
class Job extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $jsonable = ['contents'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'location',
        'startdate'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'required|unique:kori_jobadvertisement_jobs',
        'categories' => 'required',
        'is_active' => 'boolean',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kori_jobadvertisement_jobs';

    /**
     * Relations
     */
    public $belongsToMany = [
        'categories' => [
            'KoRi\JobAdvertisement\Models\JobCats',
            'table' => 'kori_jobadvertisement_p_jobs_cats',
            'order' => 'name',
            'key' => 'job_id',
            'otherKey' => 'cat_id',
            'pivot' => ['job_id', 'cat_id']
        ],
    ];

}
