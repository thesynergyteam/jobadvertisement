<?php

namespace KoRi\JobAdvertisement\Models;

use Model;

/**
 * Model
 */
class JobCats extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'name'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required|unique:kori_jobadvertisement_jobs_cats'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kori_jobadvertisement_jobs_cats';

}
