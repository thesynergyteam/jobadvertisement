<?php

namespace KoRi\JobAdvertisement\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class JobCats extends Controller {

    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $requiredPermissions = ['kori.jobadvertisement.access_jobs'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('KoRi.JobAdvertisement', 'jobadvertisement', 'jobcats');
    }

}
