<?php

return [
    'plugin' => [
        'name' => 'Állásajánlat',
        'description' => 'Az oldalon található állásajánlatok tartalmának kezelése alkalmas plugin.',
        'manage_jobs' => 'Ajánlatok kezelése',
    ],
    'navigation' => [
        'jobadvertisement' => 'Állásajánlatok',
        'jobs' => 'Állások',
        'jobcats' => 'Kategóriák',
    ],
    'permissions' => [
        'tab' => 'Állásajánlat kezelő',
        'access_settings' => 'Beállítások kezelése',
        'access_jobs' => 'Állásajánlatok kezelése',
    ],

];
