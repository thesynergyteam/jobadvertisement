<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKoriJobadvertisementJobs extends Migration
{
    public function up()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->string('headerimage', 256)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->dropColumn('headerimage');
        });
    }
}
