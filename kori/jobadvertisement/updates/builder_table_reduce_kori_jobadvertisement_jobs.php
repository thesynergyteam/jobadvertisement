<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableReduceKoriJobadvertisementJobs extends Migration
{
    public function up()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->dropColumn(['description','tasks','requirements','preferences','offers']);
        });
    }

    public function down()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->text('description');
            $table->text('tasks')->nullable();
            $table->text('requirements')->nullable();
            $table->text('preferences')->nullable();
            $table->text('offers')->nullable();
        });
    }
}