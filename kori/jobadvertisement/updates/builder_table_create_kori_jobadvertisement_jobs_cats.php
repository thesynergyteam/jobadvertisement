<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKoriJobadvertisementJobsCats extends Migration
{
    public function up()
    {
        Schema::create('kori_jobadvertisement_jobs_cats', function($table)
        {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('slug', 256);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kori_jobadvertisement_jobs_cats');
    }
}
