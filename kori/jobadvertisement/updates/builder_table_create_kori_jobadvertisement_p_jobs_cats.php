<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKoriJobadvertisementPJobsCats extends Migration
{
    public function up()
    {
        Schema::create('kori_jobadvertisement_p_jobs_cats', function($table)
        {
            $table->integer('job_id');
            $table->integer('cat_id');
            $table->primary(['job_id','cat_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kori_jobadvertisement_p_jobs_cats');
    }
}