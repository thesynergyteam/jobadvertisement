<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableExtendKoriJobadvertisementJobs extends Migration
{
    public function up()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->text('contents')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kori_jobadvertisement_jobs', function($table)
        {
            $table->dropColumn(['contents']);
        });

    }
}