<?php namespace KoRi\JobAdvertisement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKoriJobadvertisementJobs extends Migration
{
    public function up()
    {
        Schema::create('kori_jobadvertisement_jobs', function($table)
        {
            $table->increments('id');
            $table->string('title', 256);
            $table->string('slug', 256);
            $table->text('description');
            $table->text('tasks')->nullable();
            $table->text('requirements')->nullable();
            $table->text('preferences')->nullable();
            $table->text('offers')->nullable();
            $table->string('location', 256)->nullable();
            $table->string('startdate')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kori_jobadvertisement_jobs');
    }
}
